package com.example.mvvmtest.data.remote;

import com.example.mvvmtest.model.Characters;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by maranguren on 13/04/2016.
 */
public interface APIService {

    @GET("characters")
    Call<Characters> getData();

}
