package com.example.mvvmtest.data.remote;


import android.support.annotation.NonNull;

import com.example.mvvmtest.utils.Algorithms;
import com.example.mvvmtest.utils.KeyConstants;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by maranguren on 13/04/2016.
 */
public class RestService {

    public static final String KEY_APIKEY = "apikey";
    public static final String KEY_HASH = "hash";
    public static final String KEY_TS = "ts";
    private static final String API_URL = "http://gateway.marvel.com:80/v1/public/";
    private static APIService apiService = null;

    public static synchronized APIService getInstance() {

        if (apiService == null) {
            OkHttpClient.Builder httpClient = getOkHttpClient();


            Retrofit retrofit = new Retrofit.Builder().client(httpClient.build()).baseUrl(API_URL).addConverterFactory(
                    GsonConverterFactory.create()).build();

            apiService = retrofit.create(APIService.class);

        }

        return apiService;
    }

    public static OkHttpClient.Builder getOkHttpClient() {
        // Okhttp builder
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        // Add loggin interceptor only in debug mode
        HttpLoggingInterceptor logging = getLoggingInterceptor();
        httpClient.addInterceptor(logging);

        // Add the keys to every request
        Interceptor requestInterceptor = getRequestInterceptor();
        httpClient.addInterceptor(requestInterceptor);
        return httpClient;
    }

    @NonNull
    private static HttpLoggingInterceptor getLoggingInterceptor() {
        // Add logging interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }


    private static Interceptor getRequestInterceptor() {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                String ts = getTimeStamp();
                String hash = getHash(ts);
                HttpUrl url = request.url().newBuilder().addQueryParameter(KEY_APIKEY,
                        KeyConstants.API_KEY).addQueryParameter(KEY_TS, ts).addQueryParameter(KEY_HASH, hash).build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        };
    }

    private static String getTimeStamp() {
        return String.valueOf(Calendar.getInstance().getTimeInMillis());
    }

    private static String getHash(String ts) {
        return Algorithms.MD5(ts + KeyConstants.SECRET_KEY + KeyConstants.API_KEY);
    }
}
