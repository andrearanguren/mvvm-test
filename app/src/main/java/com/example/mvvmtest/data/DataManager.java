package com.example.mvvmtest.data;

import com.example.mvvmtest.data.remote.RestService;
import com.example.mvvmtest.model.Characters;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by maranguren on 21/04/2016.
 */
public class DataManager {

    public static void getData(final Callback<Characters> callback) {
        Call<Characters> call = RestService.getInstance().getData();
        call.enqueue(new retrofit2.Callback<Characters>() {
            @Override
            public void onResponse(Call<Characters> call, Response<Characters> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Characters> call, Throwable t) {
                callback.onError();
            }
        });
    }
}
