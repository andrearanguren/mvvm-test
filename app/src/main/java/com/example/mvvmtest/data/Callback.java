package com.example.mvvmtest.data;

/**
 * Created by maranguren on 21/04/2016.
 */
public interface Callback<T> {

    void onSuccess(T data);

    void onError();

}
