package com.example.mvvmtest.view.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mvvmtest.R;
import com.example.mvvmtest.databinding.ItemCharacterBinding;
import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.viewmodel.CharacterViewModel;

import java.util.List;

/**
 * Created by maranguren on 20/04/2016.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.CharacterViewHolder> {

    private final Context mContext;
    private List<Character> mCharacters;


    public MainAdapter(Context context, List<Character> characters) {
        mContext = context;
        mCharacters = characters;
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCharacterBinding postBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_character, parent, false);
        return new CharacterViewHolder(postBinding);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        ItemCharacterBinding postBinding = holder.binding;
        postBinding.setViewModel(new CharacterViewModel(mContext, mCharacters.get(position)));
        postBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }

    public void setItems(List<Character> characters) {
        mCharacters = characters;
        notifyDataSetChanged();
    }


    public static class CharacterViewHolder extends RecyclerView.ViewHolder {
        final ItemCharacterBinding binding;

        public CharacterViewHolder(ItemCharacterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


    }
}
