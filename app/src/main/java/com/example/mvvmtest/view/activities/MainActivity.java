package com.example.mvvmtest.view.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mvvmtest.R;
import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.view.adapters.MainAdapter;
import com.example.mvvmtest.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements MainViewModel.DataListener, SearchView
        .OnQueryTextListener {

    @Bind(R.id.main_recycler_view)
    RecyclerView mRecyclerView;
    @Bind(R.id.progress_bar)
    ProgressBar mProgressBar;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    List<Character> mCharacters = new ArrayList<>();
    private MainAdapter adapter;
    private MainViewModel mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        setupRecyclerView();
        mViewModel = new MainViewModel(this);
        getList();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        return true;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.main_menu, menu);
//
//        final MenuItem item = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(this);
//    }

    private void getList() {
        mViewModel.getList();
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void setupRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new MainAdapter(this, mCharacters);
        mRecyclerView.setAdapter(adapter);
    }


    @Override
    public void onListLoaded(List<Character> characters) {
        mProgressBar.setVisibility(View.GONE);
        mCharacters = characters;
        adapter.setItems(mCharacters);

        startCounter();
    }

    @Override
    public void onListFiltered(List<Character> characters) {
        adapter.setItems(characters);
        mRecyclerView.scrollToPosition(0);
    }

    private void startCounter() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mCharacters.get(0).name = "Cosa nueva";
            }
        }, 2000);
    }

    @Override
    public void onErrorLoading() {
        mProgressBar.setVisibility(View.GONE);
        Log.d("DATA", "ERROR");
        //TODO: Change

        Toast.makeText(MainActivity.this, "Error loafing data", Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mViewModel.filterList(newText, mCharacters);
        return true;
    }


}
