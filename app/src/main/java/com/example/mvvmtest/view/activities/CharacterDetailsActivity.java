package com.example.mvvmtest.view.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.mvvmtest.R;
import com.example.mvvmtest.databinding.ActivityCharacterDetailsBinding;
import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.model.Comic;
import com.example.mvvmtest.view.adapters.ComicAdapter;
import com.example.mvvmtest.viewmodel.CharacterDetailsViewModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CharacterDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_CHARACTER_KEY = "ExtraCharacterKey";

    @Bind(R.id.comics_recycler_view)
    RecyclerView mRecyclerView;
    private Character character;

    public static Intent getStartIntent(Context context, Character character) {
        Intent intent = new Intent(context, CharacterDetailsActivity.class);
        intent.putExtra(EXTRA_CHARACTER_KEY, character);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCharacterDetailsBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_character_details);
        ButterKnife.bind(this);
        character = getIntent().getParcelableExtra(EXTRA_CHARACTER_KEY);
        CharacterDetailsViewModel viewModel = new CharacterDetailsViewModel(this, character);
        binding.setViewModel(viewModel);
        setupRecyclerView(character.comics.items);


    }


    public void setupRecyclerView(List<Comic> comics) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ComicAdapter adapter = new ComicAdapter(comics);
        mRecyclerView.setAdapter(adapter);
    }
}
