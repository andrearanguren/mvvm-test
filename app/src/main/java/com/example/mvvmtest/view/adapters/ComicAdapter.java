package com.example.mvvmtest.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mvvmtest.R;
import com.example.mvvmtest.databinding.ItemComicBinding;
import com.example.mvvmtest.model.Comic;

import java.util.List;

/**
 * Created by maranguren on 20/04/2016.
 */
public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ComicViewHolder> {


    private List<Comic> mComics;


    public ComicAdapter(List<Comic> characters) {
        mComics = characters;
    }

    @Override
    public ComicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemComicBinding postBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_comic, parent, false);
        return new ComicViewHolder(postBinding);
    }

    @Override
    public void onBindViewHolder(ComicViewHolder holder, int position) {
        ItemComicBinding postBinding = holder.binding;
        postBinding.setComic(mComics.get(position));
        postBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mComics.size();
    }


    public static class ComicViewHolder extends RecyclerView.ViewHolder {
        final ItemComicBinding binding;

        public ComicViewHolder(ItemComicBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


    }
}
