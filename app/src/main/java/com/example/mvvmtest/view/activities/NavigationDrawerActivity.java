package com.example.mvvmtest.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mvvmtest.R;

public class NavigationDrawerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
    }
}
