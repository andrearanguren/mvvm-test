package com.example.mvvmtest.bindingadapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by maranguren on 25/04/2016.
 */
public class BindingAdapters {

    @BindingAdapter("bind:imageUrl")
    public static void setImageUrl(ImageView view, String imageUrl) {
        Picasso.with(view.getContext()).load(imageUrl).fit().into(view);
    }

}
