package com.example.mvvmtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by maranguren on 27/04/2016.
 */
public class Comics implements Parcelable {
    public static final Parcelable.Creator<Comics> CREATOR = new Parcelable.Creator<Comics>() {
        @Override
        public Comics createFromParcel(Parcel source) {
            return new Comics(source);
        }

        @Override
        public Comics[] newArray(int size) {
            return new Comics[size];
        }
    };
    public List<Comic> items;

    public Comics() {
    }

    protected Comics(Parcel in) {
        this.items = in.createTypedArrayList(Comic.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(items);
    }
}
