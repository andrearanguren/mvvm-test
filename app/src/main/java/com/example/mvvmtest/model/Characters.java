package com.example.mvvmtest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by maranguren on 25/04/2016.
 */
public class Characters {

    public Data data;

    public class Data {
        @SerializedName("results")
        public List<Character> characters;
    }
}
