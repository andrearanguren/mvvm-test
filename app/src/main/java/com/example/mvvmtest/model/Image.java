package com.example.mvvmtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maranguren on 27/04/2016.
 */
public class Image implements Parcelable {

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
    @SerializedName("path")
    public String imageUrl;
    public String extension;

    public Image() {
    }

    protected Image(Parcel in) {
        this.imageUrl = in.readString();
        this.extension = in.readString();
    }

    public String getFullUrl() {
        return imageUrl + "." + extension;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageUrl);
        dest.writeString(this.extension);
    }
}
