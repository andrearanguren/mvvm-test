package com.example.mvvmtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maranguren on 13/04/2016.
 */
public class Character implements Parcelable {

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel source) {
            return new Character(source);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };
    public Long id;
    public String name;
    public String description;
    @SerializedName("thumbnail")
    public Image image;
    public Comics comics;



    public Character() {
    }

    protected Character(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.description = in.readString();
        this.image = in.readParcelable(Image.class.getClassLoader());
        this.comics = in.readParcelable(Comics.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeParcelable(this.image, flags);
        dest.writeParcelable(this.comics, flags);
    }
}
