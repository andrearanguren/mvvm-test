package com.example.mvvmtest.viewmodel;

import android.content.Context;
import android.view.View;

import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.view.activities.CharacterDetailsActivity;

/**
 * Created by maranguren on 13/04/2016.
 */
public class CharacterDetailsViewModel {
    private Context context;
    private Character character;

    public CharacterDetailsViewModel(Context context, Character character) {
        this.context = context;
        this.character = character;
    }

    public String getTitle() {
        return character.name;
    }

    public String getDescription() {

        return character.description;
    }

    public String getImageUrl() {
        return character.image.getFullUrl();
    }


    public View.OnClickListener onItemClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(CharacterDetailsActivity.getStartIntent(context, character));
            }
        };
    }

}
