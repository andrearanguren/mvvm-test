package com.example.mvvmtest.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import com.example.mvvmtest.R;
import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.view.activities.CharacterDetailsActivity;

/**
 * Created by maranguren on 13/04/2016.
 */
public class CharacterViewModel {
    private Context context;
    private Character character;

    public CharacterViewModel(Context context, Character character) {
        this.context = context;
        this.character = character;
    }

    public String getPostTitle() {
        return character.name;
    }

    public String getPostText() {

        return character.description;
    }

    public String getImageUrl() {
        //return "http://img01.deviantart.net/45c8/a/large/wallpaper/wminimal/animated_kc_flash_symbol.jpg";
        return character.image.getFullUrl();
    }


    public View.OnClickListener onMoreClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }

    public View.OnClickListener onItemClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = CharacterDetailsActivity.getStartIntent(context, character);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity) context, v.findViewById(R.id.image_view),
                                "characterImage");

                context.startActivity(intent, options.toBundle());
            }
        };
    }

}
