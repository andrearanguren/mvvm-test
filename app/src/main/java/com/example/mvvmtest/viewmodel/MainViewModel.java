package com.example.mvvmtest.viewmodel;

import com.example.mvvmtest.data.Callback;
import com.example.mvvmtest.data.DataManager;
import com.example.mvvmtest.model.Character;
import com.example.mvvmtest.model.Characters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maranguren on 13/04/2016.
 */
public class MainViewModel {

    private final DataListener mListener;

    public MainViewModel(DataListener listener) {
        mListener = listener;
    }

    public void getList() {
        DataManager.getData(new Callback<Characters>() {
            @Override
            public void onSuccess(Characters data) {
                mListener.onListLoaded(data.data.characters);
            }

            @Override
            public void onError() {
                mListener.onErrorLoading();
            }
        });
    }

    public void filterList(String query, List<Character> characters) {
        query = query.toLowerCase();

        final List<Character> filteredModelList = new ArrayList<>();
        for (Character model : characters) {
            final String text = model.name.toLowerCase();
            final String description = model.description;

            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        mListener.onListFiltered(filteredModelList);

    }


    public interface DataListener {
        void onListLoaded(List<Character> characters);

        void onListFiltered(List<Character> characters);

        void onErrorLoading();
    }

}
